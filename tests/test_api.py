import asyncio
from contextlib import AsyncExitStack
from typing import AsyncGenerator

import fastapi
import httpx
import pytest

from hh_quote_cache.api import api_app_lifespan, build_app
from hh_quote_cache.db import TDBPool
from hh_quote_cache.models import Base
from hh_quote_cache.settings import PairConfig, Settings
from hh_quote_cache.updater import Updater


@pytest.fixture()
async def api_app(settings: Settings, acm: AsyncExitStack) -> AsyncGenerator[fastapi.FastAPI, None]:
    app = build_app(settings)
    async with api_app_lifespan(settings, app):
        yield app


@pytest.fixture()
async def api_cli(api_app: fastapi.FastAPI) -> AsyncGenerator[httpx.AsyncClient, None]:
    async with httpx.AsyncClient(app=api_app, base_url="http://localhost:8080") as cli:
        yield cli


async def test_ping(api_cli: httpx.AsyncClient) -> None:
    resp = await api_cli.get("/ping?test=tset")
    resp.raise_for_status()
    resp_data = resp.json()
    assert resp_data["url"] == "http://localhost:8080/ping?test=tset"


async def test_quote(
    settings: Settings,
    api_app: fastapi.FastAPI,
    api_cli: httpx.AsyncClient,
    pair: PairConfig,
    acm: AsyncExitStack,
    binancedapi_mock: dict,
) -> None:
    db_pool: TDBPool = api_app.state.db

    # It's rather tricky to pass the non-env settings to `alembic`,
    # so running the SA table creation directly.
    # TODO: tests with local database;
    # TODO: also, stairway migrations test:
    # https://github.com/alvassin/alembic-quickstart/blob/master/tests/migrations/test_stairway.py
    async with db_pool() as db_sess:
        conn = await db_sess.connection()
        await conn.run_sync(Base.metadata.create_all)

    updater = Updater(settings=settings)

    # Due to in-memory db, have to pass the pool.
    updater.db_pool = db_pool

    # Make the updater run only once
    updater.stopping = True
    updater.raise_errors = True
    await asyncio.wait_for(updater.run(), timeout=1.0)

    resp = await api_cli.get("/quote?from=TST1&to=TST2&amount=123.456789&at=2023-12-07T09:40Z")
    resp.raise_for_status()
    resp_data = resp.json()
    assert resp_data["result"] == "276261.725881"
    assert resp_data["rate"] == "2237.720000000000"
    assert resp_data["age_sec"] == 12.228

    resp = await api_cli.get("/quote?from=TST2&to=TST1&amount=123.456789&at=2023-12-07T09:40Z")
    resp.raise_for_status()
    resp_data = resp.json()
    assert resp_data["result"] == "0.055171"
    assert resp_data["rate"] == "0.000446883435"
    assert resp_data["age_sec"] == 12.228

    resp = await api_cli.get("/quote?from=TST2&to=TST1&amount=123.456789&at=2023-12-07T09:41Z")
    assert resp.status_code == 404
    resp_data = resp.json()
    assert resp_data["detail"]["code"] == "quotes_outdated"
    assert resp_data["detail"]["age_sec"] == 72.228
    assert resp_data["detail"]["max_age_sec"] == 60.0

    resp = await api_cli.get("/quote?from=TST1&to=TST3&amount=123.456789&at=2023-12-07T09:40Z")
    assert resp.status_code == 404
    resp_data = resp.json()
    assert resp_data["detail"]["code"] == "quotes_not_found"
