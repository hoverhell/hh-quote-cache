import json
import logging
from collections.abc import Generator

import pytest

from hh_quote_cache.runlib import init_logs
from hh_quote_cache.settings import Settings, SettingsOptsBase


@pytest.fixture()
def _root_logger_handlers_keeper() -> Generator[None, None, None]:
    root = logging.getLogger()
    handlers = root.handlers[:]
    yield
    root.handlers[:] = handlers


@pytest.mark.usefixtures("_root_logger_handlers_keeper")
def test_dev_logs(capsys: pytest.CaptureFixture) -> None:
    init_logs(Settings(opts=SettingsOptsBase(env="dev")))
    logger = logging.getLogger("testslogger")
    logger.debug("test msg", extra=dict(x_tests="tyests"))
    outs = capsys.readouterr()
    assert not outs.out
    assert "DEBUG" in outs.err
    assert "test msg" in outs.err


@pytest.mark.usefixtures("_root_logger_handlers_keeper")
def test_prod_logs(capsys: pytest.CaptureFixture) -> None:
    init_logs(Settings(opts=SettingsOptsBase(env="prod")))
    logger = logging.getLogger("testslogger")
    logger.debug("test msg", extra=dict(x_tests="tyests"))
    outs = capsys.readouterr()
    assert not outs.out
    line = json.loads(outs.err.splitlines()[0])
    assert line["created"]
    assert line["asctime"]
    assert line["levelname"] == "DEBUG"
    assert line["name"] == "testslogger"
    assert line["message"] == "test msg"
    assert line["x_tests"] == "tyests"
