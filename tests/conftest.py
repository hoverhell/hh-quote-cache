import contextlib
import json
from collections.abc import AsyncGenerator, Generator

import aioresponses
import pytest

from hh_quote_cache.runlib import INIT_STATE
from hh_quote_cache.settings import PairConfig, Settings, SettingsOptsBase


@pytest.fixture()
def pair() -> PairConfig:
    return PairConfig(src="TST1", dst="TST2", market="binance_dapi", ticker="_BTICKER_")


@pytest.fixture(autouse=True)
def settings(pair: PairConfig) -> Settings:
    settings = Settings(
        opts=SettingsOptsBase(
            env="tests",
            pairs_config=[pair],
        ),
        # Use in-memory sqlite for simplicity.
        db_url_override="sqlite+aiosqlite://",
    )
    # Prevent further `init_all()` activity.
    INIT_STATE["settings"] = settings
    return settings


@pytest.fixture()
async def acm() -> AsyncGenerator[contextlib.AsyncExitStack, None]:
    async with contextlib.AsyncExitStack() as _acm:
        yield _acm


@pytest.fixture(autouse=True)
def aior() -> Generator[aioresponses.aioresponses, None, None]:
    """Disable `aiohttp`'s network calls by automatically applying the mocker fixture"""
    with aioresponses.aioresponses() as mocked:
        yield mocked


@pytest.fixture()
def binancedapi_mock(aior: aioresponses.aioresponses, pair: PairConfig) -> dict:
    assert pair.market == "binance_dapi"
    assert pair.ticker == "_BTICKER_"
    item = {"symbol": "_BTICKER_", "ps": "BTICK", "price": "2237.72", "time": 1701941987772}
    aior.get(
        "https://dapi.binance.com/dapi/v1/ticker/price?symbol=_BTICKER_",
        body=json.dumps([item], separators=(",", ":")),
    )
    return item
