import datetime
from contextlib import AsyncExitStack
from decimal import Decimal

import pytest

from hh_quote_cache.settings import PairConfig
from hh_quote_cache.updater import BinanceDAPIHandler, Updater


@pytest.mark.asyncio()
async def test_binancedapi_handler(binancedapi_mock: dict, acm: AsyncExitStack, pair: PairConfig) -> None:
    handler = BinanceDAPIHandler()
    await handler.initialize(acm)
    quote, dt = await handler.get_quote(pair)
    assert isinstance(quote, Decimal)
    assert str(quote) == "2237.72"
    assert dt == datetime.datetime.fromisoformat("2023-12-07T09:39:47.772Z")


@pytest.mark.asyncio()
async def test_updater_init(settings, acm) -> None:
    updater = Updater(settings=settings)
    await updater.initialize(acm)
    await updater.initialize(acm)


@pytest.mark.asyncio()
async def test_handler_init(settings, acm) -> None:
    handler = BinanceDAPIHandler()
    await handler.initialize(acm)
    await handler.initialize(acm)
