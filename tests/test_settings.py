import pytest

from hh_quote_cache.settings import (
    PairConfig,
    Settings,
    SettingsOptsBase,
    SettingsOptsEnv,
)


def test_settings_test_env(settings: Settings) -> None:
    assert settings.opts.env == "tests"


def test_settings_default_env() -> None:
    assert Settings().opts.env == "dev"


def test_settings_override_env(monkeypatch: pytest.MonkeyPatch) -> None:
    monkeypatch.setenv("HHQC_ENV", "prod")
    assert Settings().opts.env == "prod"


def test_settings_yaml(monkeypatch: pytest.MonkeyPatch) -> None:
    assert Settings().opts.pairs_config[0].dst == "USD"
    cfg_text = "- {src: TST1, dst: TST2, market: binance_dapi, ticker: TST3}"  # noqa: FS003
    monkeypatch.setenv("HHQC_PAIRS_CONFIG", cfg_text)
    cfg = Settings().opts.pairs_config
    assert cfg == [PairConfig(src="TST1", dst="TST2", market="binance_dapi", ticker="TST3")]


def test_clean_settings(monkeypatch: pytest.MonkeyPatch) -> None:
    monkeypatch.setenv("HHQC_ENV", "prod")
    assert SettingsOptsEnv().env == "prod"
    assert SettingsOptsBase().env == "dev"
