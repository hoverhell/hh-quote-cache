# FROM python:3.11-slim-bookworm AS base
FROM python@sha256:cfd7ed5c11a88ce533d69a1da2fd932d647f9eb6791c5b4ddce081aedf7f7876 AS base

ENV \
    DEBIAN_FRONTEND=noninteractive \
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_HOME="/opt/poetry"

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install --no-install-recommends \
        curl git build-essential tini \
    && \
    apt-get -y purge --auto-remove -o APT::AutoRemove::RecommendsImportant=false && \
    apt-get -y clean && rm -rf /var/lib/apt/lists/*

RUN python -m pip install --upgrade poetry~=1.7.1


FROM base AS install

WORKDIR /opt/code
COPY pyproject.toml poetry.lock ./
RUN poetry install --only=main --no-root


FROM install as app

COPY . .
RUN poetry install --only-root

RUN mkdir --parents /opt/code && \
    groupadd --gid=1000 app && \
    useradd --uid=1000 --home-dir=/opt/code --gid=app app && \
    chown app:app -R /opt/code
USER app
