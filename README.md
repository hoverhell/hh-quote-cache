# Currency Converter #

Service that caches currency conversion quotes and provides API for conversion.

WORK IN PROGRESS

API call example: `curl "http://localhost:8080/quote?from=USD&to=ETH&amount=9000.123456"`

Response example: `{"result":"4.073133","rate":"0.000452564109","age_sec":12.345}`.

API call example with additional parameters:

    curl "http://localhost:8080/quote?from=USD&to=ETH&amount=9000.123456&at=2038-02-03T04:05:06Z&max_age_sec=631152000"

To run the service locally, use `docker-compose up`.
To change the default published API port and other settings, use environment variables, e.g.:

    APP_PORT=12345 HHQC_ENV=dev docker-compose up --build

For the full list of settings, see `src/hh_quote_cache/settings.py` (note the `HHQC_` prefix).


## Development ##

Prepare / rebuild the environment starting from pyenv:

    pyenv install 3.11 && \
    pyenv local 3.11 && \
    python -c 'import sys; assert sys.version_info > (3, 10)' && \
    python -m pip install --upgrade pip~=23.3.1 && \
    python -m pip install --upgrade poetry~=1.7.1 && \
    (python -m poetry env remove 3.11 || true) && \
    python -m poetry env use 3.11 && \
    poetry install --sync

To format the code and run tests, use `poetry run hyd` (see also: `hydpytest`, `hydblack`, etc.).

Run migrations on the local database:

    poetry run hh-quote-cache-alembic upgrade head

Generate a a new migration:

    poetry run hh-quote-cache-alembic revision --autogenerate -m "something"
