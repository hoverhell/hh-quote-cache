import typer

from hh_quote_cache.api import main_cli as api_main_cli
from hh_quote_cache.runlib import init_all
from hh_quote_cache.updater import main_cli as updater_main_cli

CLI_APP = typer.Typer()
CLI_APP.command("api")(api_main_cli)
CLI_APP.command("updater")(updater_main_cli)
# TODO: find a good way to add pass-through `alembic` command.


def main() -> None:
    init_all()
    CLI_APP()
