from typing import Any, Literal

import pydantic
import pydantic_settings
import yaml

TEnvName = Literal["dev", "tests", "devrun", "prod"]
TMarket = Literal["binance_dapi"]


class PairConfig(pydantic.BaseModel):
    src: str
    dst: str
    market: TMarket
    ticker: str
    interval_sec: float | None = None


class CustomEnvSettingsSource(pydantic_settings.EnvSettingsSource):
    def decode_complex_value(self, field_name: str, field: pydantic.fields.FieldInfo, value: Any) -> Any:
        return yaml.safe_load(value)


class SettingsOptsBase(pydantic_settings.BaseSettings):
    """Overridable key-value settings, base version that only reads init arguments"""

    model_config = pydantic_settings.SettingsConfigDict(env_prefix="hhqc_")

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: type[pydantic_settings.BaseSettings],
        init_settings: pydantic_settings.PydanticBaseSettingsSource,
        env_settings: pydantic_settings.PydanticBaseSettingsSource,
        dotenv_settings: pydantic_settings.PydanticBaseSettingsSource,
        file_secret_settings: pydantic_settings.PydanticBaseSettingsSource,
    ) -> tuple[pydantic_settings.PydanticBaseSettingsSource, ...]:
        return (init_settings,)

    env: TEnvName = "dev"  # `HHQC_ENV`

    api_bind: str = "0.0.0.0"
    api_port: int = 8080
    api_deploy_workers: int = 4
    api_dev_reload: bool = True

    default_ttl_sec: float = 60.0
    update_interval_sec: float = 30.0
    storage_ttl_days: float = 7.1
    cleanup_interval_sec: float = 3600.0

    # TODO: support YAML instead of JSON setting.
    pairs_config: list[PairConfig] = [
        PairConfig(src="ETH", dst="USD", market="binance_dapi", ticker="ETHUSD_PERP"),
        PairConfig(src="BTC", dst="USD", market="binance_dapi", ticker="BTCUSD_PERP"),
    ]

    postgres_host: str = "localhost"
    postgres_port: str = "5432"
    postgres_user: str = "postgres"
    postgres_password: str = "1"
    postgres_db: str = "postgres"


class SettingsOptsEnv(SettingsOptsBase):
    """Overridable settings class that also loads values from `os.environ`"""

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: type[pydantic_settings.BaseSettings],
        init_settings: pydantic_settings.PydanticBaseSettingsSource,
        env_settings: pydantic_settings.PydanticBaseSettingsSource,
        dotenv_settings: pydantic_settings.PydanticBaseSettingsSource,
        file_secret_settings: pydantic_settings.PydanticBaseSettingsSource,
    ) -> tuple[pydantic_settings.PydanticBaseSettingsSource, ...]:
        # Ignoring `dotenv_settings` and `file_secret_settings`,
        # customizing `env_settings`
        return (init_settings, CustomEnvSettingsSource(settings_cls))


class Settings(pydantic.BaseModel):
    opts: SettingsOptsBase = pydantic.Field(default_factory=SettingsOptsEnv)
    pg_sa_prefix: str = "postgresql+asyncpg://"
    db_url_override: str | None = None

    @property
    def db_url(self) -> str:
        if self.db_url_override is not None:
            return self.db_url_override
        return (
            f"{self.pg_sa_prefix}{self.opts.postgres_user}:{self.opts.postgres_password}@"
            f"{self.opts.postgres_host}:{self.opts.postgres_port}/{self.opts.postgres_db}"
        )
