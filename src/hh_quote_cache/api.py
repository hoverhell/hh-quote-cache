import contextlib
import functools
from collections.abc import AsyncGenerator

import fastapi
import uvicorn

from hh_quote_cache.api_handlers import API_ROUTER
from hh_quote_cache.db import db_session_acm
from hh_quote_cache.runlib import init_all
from hh_quote_cache.settings import Settings


@contextlib.asynccontextmanager
async def api_app_lifespan(settings: Settings, app: fastapi.FastAPI) -> AsyncGenerator[None, None]:
    init_all(settings)
    async with contextlib.AsyncExitStack() as lifespan_acm:
        app.state.acm = lifespan_acm
        app.state.db = await lifespan_acm.enter_async_context(db_session_acm(settings))
        yield
    app.state.db = None
    app.state.acm = None


def build_app(settings: Settings | None = None) -> fastapi.FastAPI:
    if settings is None:
        settings = Settings()
    app = fastapi.FastAPI(lifespan=functools.partial(api_app_lifespan, settings))
    app.state.settings = settings
    app.include_router(API_ROUTER)
    return app


API_APP = build_app()


def main_run(settings: Settings | None = None) -> None:
    app: fastapi.FastAPI | str
    if settings is None:
        settings = Settings()
        # Needed as string for `reload` (but might still not work):
        app = "hh_quote_cache.api:API_APP"
    else:
        # `reload` will not work.
        app = build_app(settings)

    if settings.opts.env == "dev":
        config = uvicorn.Config(
            app,
            host=settings.opts.api_bind,
            port=settings.opts.api_port,
            reload=settings.opts.api_dev_reload,
        )
    else:
        config = uvicorn.Config(
            app,
            host=settings.opts.api_bind,
            port=settings.opts.api_port,
            workers=settings.opts.api_deploy_workers,
        )

    server = uvicorn.Server(config)
    server.run()


def main_cli() -> None:
    """
    Serve the HTTP API.
    Configuration is through the environment variables.
    """
    init_all()
    main_run()
