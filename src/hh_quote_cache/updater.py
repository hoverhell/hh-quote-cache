import asyncio
import contextlib
import dataclasses
import datetime
import json
import logging
from collections.abc import AsyncGenerator, Sequence
from decimal import Decimal
from typing import ClassVar

import aiohttp
import aiohttp_retry

from hh_quote_cache.alembic.main import run_auto_migrate
from hh_quote_cache.db import TDBConn, TDBPool, db_session_acm
from hh_quote_cache.models import Quote
from hh_quote_cache.runlib import init_all
from hh_quote_cache.settings import PairConfig, Settings, TMarket
from hh_quote_cache.utils import dt_now, floor_dt_to_interval

LOGGER = logging.getLogger(__name__)


class MarketHandler:
    market: ClassVar[TMarket]

    async def initialize(self, acm: contextlib.AsyncExitStack) -> None:
        pass

    async def get_quote(self, pair: PairConfig) -> tuple[Decimal, datetime.datetime | None]:
        raise NotImplementedError


@dataclasses.dataclass(kw_only=True)
class BinanceDAPIHandler(MarketHandler):
    market: ClassVar[TMarket] = "binance_dapi"
    retry_options: aiohttp_retry.RetryOptionsBase = aiohttp_retry.ExponentialRetry(
        attempts=3, start_timeout=0.1, max_timeout=2.5
    )
    url: str = "https://dapi.binance.com/dapi/v1/ticker/price"

    _http_session: aiohttp.ClientSession | None = None

    @contextlib.asynccontextmanager
    async def _manage_session(self) -> AsyncGenerator[None, None]:
        if self._http_session is not None:
            yield
            return

        try:
            async with aiohttp.ClientSession() as sess:
                self._http_session = sess
                yield
        finally:
            self._http_session = None

    async def initialize(self, acm: contextlib.AsyncExitStack) -> None:
        # To consider: persistent websocket connection.
        # Would reduce latency, but complicate the retries.
        await acm.enter_async_context(self._manage_session())

    async def get_quote(self, pair: PairConfig) -> tuple[Decimal, datetime.datetime | None]:
        if self._http_session is None:
            raise Exception("Must be initialized")

        symbol = pair.ticker
        retry_client = aiohttp_retry.RetryClient(self._http_session, retry_options=self.retry_options)
        async with retry_client.get(self.url, params=dict(symbol=symbol)) as resp:
            resp_text = await resp.text()
            try:
                resp.raise_for_status()
            except aiohttp.ClientResponseError as exc:
                exc.message = f"{exc.message}; {resp_text[:1000]=!r}"
                raise
            resp_data = json.loads(resp_text)
        LOGGER.debug("%s resp for %r symbol=%r: %r", self.__class__.__name__, self.url, symbol, resp_data)
        resp_item = resp_data[0]
        quote: str = resp_item["price"]
        ts_msec: int = resp_item["time"]
        ts_dt = datetime.datetime.fromtimestamp(ts_msec / 1000, datetime.timezone.utc)
        return Decimal(quote), ts_dt


@dataclasses.dataclass(kw_only=True)
class Updater:
    settings: Settings
    model: type[Quote] = Quote
    market_handlers: Sequence[type[MarketHandler]] = (BinanceDAPIHandler,)
    db_pool: TDBPool | None = None
    stopping: bool = False
    raise_errors: bool = False

    def __post_init__(self) -> None:
        market_to_handler_cls = {handler.market: handler for handler in self.market_handlers}
        if len(market_to_handler_cls) != len(self.market_handlers):
            raise ValueError("Non-unique market_handlers")
        self._market_to_handler_cls = market_to_handler_cls
        self._market_to_handler: dict[TMarket, MarketHandler] = {}

    @staticmethod
    def get_sleep_interval_sec(
        last_update: datetime.datetime, update_interval_sec: float, now: datetime.datetime | None = None
    ) -> float:
        """
        It's basically time to `last_update_ts + update_interval_sec`",
        but rounded down to time of the day.

        >>> Updater.get_sleep_interval_sec(
        ...     datetime.datetime.fromisoformat("2023-04-05T06:07:08Z"),
        ...     30,
        ...     now=datetime.datetime.fromisoformat("2023-04-05T06:07:12Z"),
        ... )
        18.0
        >>> Updater.get_sleep_interval_sec(
        ...     datetime.datetime.fromisoformat("2023-04-05T06:07:08Z"),
        ...     30,
        ...     now=datetime.datetime.fromisoformat("2023-04-05T06:07:31Z"),
        ... )
        0
        """
        last_update_rounded = floor_dt_to_interval(last_update, update_interval_sec)
        next_update = last_update_rounded + datetime.timedelta(seconds=update_interval_sec)
        if now is None:
            now = dt_now()
        result = (next_update - now).total_seconds()
        if result <= 0:
            return 0
        return result

    async def run_cleanup_once(self, now: datetime.datetime | None = None) -> None:
        if now is None:
            now = dt_now()

        cutoff = now - datetime.timedelta(days=self.settings.opts.storage_ttl_days)
        query = self.model.query_delete_old(cutoff)

        async with self._db_conn_acm() as db_conn:
            cur = await db_conn.execute(query)
            res = cur.rowcount

        LOGGER.info("Cleanup result: cutoff=%r, res=%r", cutoff.isoformat(), res)

    async def run_cleanup(self) -> None:
        while True:
            await self.run_cleanup_once()

            if self.stopping:
                break
            await asyncio.sleep(self.settings.opts.cleanup_interval_sec)

    async def run_pair_once(self, pair: PairConfig) -> None:
        handler = self._market_to_handler[pair.market]
        quote, quote_ts = await handler.get_quote(pair)

        # Assuming handler doesn't take much time to return the quote after obtaining it.
        quote_ts = quote_ts or dt_now()
        quote_obj = self.model(src=pair.src, dst=pair.dst, timestamp=quote_ts, quote=quote)

        async with self._db_conn_acm() as db_conn:
            await quote_obj.save_or_update(db_conn)

    async def run_pair(self, pair: PairConfig) -> None:
        update_interval_sec = pair.interval_sec or self.settings.opts.update_interval_sec
        while True:
            last_update = dt_now()

            try:
                await self.run_pair_once(pair)
            except Exception:
                LOGGER.exception("Failed updating pair=%r", pair)
                if self.raise_errors:
                    raise

            sleep_interval_sec = self.get_sleep_interval_sec(
                last_update=last_update, update_interval_sec=update_interval_sec
            )
            LOGGER.debug("Updater pair=%r: next check in %.3fs", pair, sleep_interval_sec)

            if self.stopping:
                break
            await asyncio.sleep(sleep_interval_sec)

    @contextlib.asynccontextmanager
    async def _ensure_db(self) -> AsyncGenerator[None, None]:
        if self.db_pool is not None:
            yield
            return

        try:
            async with db_session_acm(self.settings) as db_pool:
                self.db_pool = db_pool
                yield
        finally:
            self.db_pool = None

    async def _initialize_handlers(self, acm: contextlib.AsyncExitStack) -> None:
        if self._market_to_handler:
            return

        pairs = self.settings.opts.pairs_config
        markets = {pair.market for pair in pairs}
        handlers = {market: self._market_to_handler_cls[market]() for market in markets}
        self._market_to_handler.update(handlers)
        asyncio.gather(*[handler.initialize(acm) for handler in handlers.values()])

    async def initialize(self, acm: contextlib.AsyncExitStack) -> None:
        await acm.enter_async_context(self._ensure_db())
        await self._initialize_handlers(acm)

    @contextlib.asynccontextmanager
    async def _db_conn_acm(self, autocommit: bool = True) -> AsyncGenerator[TDBConn, None]:
        db_pool = self.db_pool
        if db_pool is None:
            raise Exception("Should `initialize` first")

        async with db_pool() as db_conn:
            yield db_conn
            if autocommit:
                await db_conn.commit()

    async def run(self) -> None:
        pairs = self.settings.opts.pairs_config
        acm = contextlib.AsyncExitStack()
        async with acm:
            await self.initialize(acm)
            # Run forever
            await asyncio.gather(self.run_cleanup(), *[self.run_pair(pair) for pair in pairs])


def main_cli(auto_migrate: bool = False) -> None:
    """
    Run the updater process.
    Configuration is through the environment variables.
    """
    init_all()
    if auto_migrate:
        run_auto_migrate()
    updater = Updater(settings=Settings())
    asyncio.run(updater.run())
