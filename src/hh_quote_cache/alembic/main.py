import logging
import sys
from collections.abc import Sequence

import alembic.config

from hh_quote_cache.runlib import init_all

LOGGER = logging.getLogger(__name__)


def alembic_config_setup(config: alembic.config.Config) -> None:
    config.set_main_option("script_location", "src/hh_quote_cache/alembic/")


def alembic_main(args: Sequence[str]) -> None:
    cli = alembic.config.CommandLine()

    options = cli.parser.parse_args(args)
    if not getattr(options, "cmd", None):
        if args:
            raise Exception("Missing: command")
        options = cli.parser.parse_args(["heads"])

    config = alembic.config.Config(cmd_opts=options)
    alembic_config_setup(config)
    cli.run_cmd(config, options)


def run_auto_migrate():
    LOGGER.info("Running auto migrate: `... alembic --raiseerr upgrade head`")
    alembic_main(["--raiseerr", "upgrade", "head"])


def main() -> None:
    init_all()
    alembic_main(sys.argv[1:])
