import asyncio

import alembic
import alembic.script
import sqlalchemy

from hh_quote_cache.db import make_db_engine
from hh_quote_cache.models import Base
from hh_quote_cache.runlib import init_logs
from hh_quote_cache.settings import Settings

TARGET_METADATA = Base.metadata


def process_revision_directives(context, revision, directives):
    """Prefix alembic migration revisions with a consecutive number"""
    migration_script = directives[0]
    assert isinstance(migration_script, alembic.operations.MigrationScript)

    head_revision = alembic.script.ScriptDirectory.from_config(context.config).get_current_head()
    if head_revision is None:
        new_rev_id = 1
    else:
        last_rev_id = int(head_revision.split("_", 1)[0].lstrip("0"))
        new_rev_id = last_rev_id + 1

    old_rev_id = migration_script.rev_id or ""
    migration_script.rev_id = f"{new_rev_id:04}_{old_rev_id[:6]}"


def do_run_migrations(
    connection: sqlalchemy.Connection, target_metadata: sqlalchemy.MetaData = TARGET_METADATA
) -> None:
    alembic.context.configure(
        compare_type=True,
        dialect_opts={"paramstyle": "named"},
        connection=connection,
        target_metadata=target_metadata,
        include_schemas=True,
        # literal_binds=True,
        version_table_schema=target_metadata.schema,
        process_revision_directives=process_revision_directives,
    )

    with alembic.context.begin_transaction():
        alembic.context.run_migrations()


async def run_async_migrations(settings: Settings) -> None:
    """In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    engine = make_db_engine(settings)

    try:
        async with engine.connect() as connection:
            await connection.run_sync(do_run_migrations)
    finally:
        await engine.dispose()


def run_migrations_online(settings: Settings) -> None:
    asyncio.run(run_async_migrations(settings))


def main() -> None:
    if alembic.context.is_offline_mode():
        raise Exception("Not supported: offline mode (`--sql`)")

    settings = Settings()
    init_logs(settings)
    run_migrations_online(settings)


if __name__ == "env_py":
    main()
