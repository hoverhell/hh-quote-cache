import logging

import coloredlogs
import pythonjsonlogger.jsonlogger

from hh_quote_cache.settings import Settings

CL_FMT = "%(asctime)s.%(msecs)03d %(levelname)-5s %(name)s %(message)s"
PJL_FMT = "%(created)s %(asctime)s %(levelname)s %(name)s %(message)s"

INIT_STATE: dict[str, Settings] = {}


def init_logs(settings: Settings, pjl_fmt: str = PJL_FMT, cl_fmt: str = CL_FMT) -> None:
    # Levels tuning
    logging.getLogger("sqlalchemy").setLevel("DEBUG")

    if settings.opts.env in ("dev", "tests"):
        coloredlogs.install(fmt=cl_fmt, level="DEBUG", milliseconds=True)
        return

    logger = logging.getLogger()

    formatter = pythonjsonlogger.jsonlogger.JsonFormatter(pjl_fmt)

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger.setLevel("DEBUG")  # filter it in post if needed
    logger.addHandler(handler)


def init_all(settings: Settings | None = None) -> None:
    if settings is None:
        settings = Settings()

    # Allow for re-calling.
    # Needed to ensure initialization when under e.g. gunicorn.
    if INIT_STATE:
        prev_settings = INIT_STATE["settings"]
        if settings != prev_settings:
            raise Exception(f"Trying to initialize with different settings: {settings=!r} != {prev_settings=!r}")
        return
    INIT_STATE["settings"] = settings

    init_logs(settings)
