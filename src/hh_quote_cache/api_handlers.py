import datetime
import decimal
import logging
from typing import Annotated, AsyncGenerator

import fastapi
import sqlalchemy
from fastapi import Depends, HTTPException, status

from hh_quote_cache.db import TDBConn, TDBPool
from hh_quote_cache.models import Quote
from hh_quote_cache.settings import Settings
from hh_quote_cache.utils import assume_utc, dt_now

LOGGER = logging.getLogger(__name__)
API_ROUTER = fastapi.APIRouter()


async def db_conn_acm(request: fastapi.Request) -> AsyncGenerator[TDBConn, None]:
    db: TDBPool = request.app.state.db
    async with db() as db_conn:
        yield db_conn


def settings_extract(request: fastapi.Request) -> Settings:
    settings: Settings = request.app.state.settings
    return settings


TDBDep = Annotated[TDBConn, fastapi.Depends(db_conn_acm)]
TSettingsDep = Annotated[Settings, Depends(settings_extract)]


@API_ROUTER.get("/ping")
async def get_ping(request: fastapi.Request, db: TDBDep) -> dict:
    if db.bind.dialect.name == "sqlite":
        db_ver_query = "select sqlite_version()"
    else:
        db_ver_query = "select version()"
    db_ver = await db.scalar(sqlalchemy.text(db_ver_query))

    result = dict(message="pong", url=str(request.url), headers=dict(request.headers), db_ver=db_ver)

    LOGGER.debug("Ping result: %r", result)
    return result


@API_ROUTER.get("/quote")
async def get_quote(
    request: fastapi.Request,
    db: TDBDep,
    settings: TSettingsDep,
    from_: Annotated[str, fastapi.Query(alias="from")],
    to: str,
    amount: decimal.Decimal,
    at: datetime.datetime | float | None = None,
    max_age_sec: float | None = None,
) -> dict:
    if isinstance(at, (int, float)):
        at = datetime.datetime.fromtimestamp(at, datetime.timezone.utc)
    elif isinstance(at, datetime.datetime):
        # Force interpret tz-naive ISO8601 as UTC.
        at = assume_utc(at)
    if max_age_sec is None:
        max_age_sec = settings.opts.default_ttl_sec

    query = Quote.query_find(from_, to, at_ts=at)
    cur = await db.execute(query)
    items = cur.fetchone()
    if not items:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=dict(code="quotes_not_found"))
    item: Quote = items[0]

    if at is None:
        at = dt_now()
    age_sec = (at - assume_utc(item.timestamp)).total_seconds()
    if age_sec > max_age_sec:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=dict(code="quotes_outdated", age_sec=age_sec, max_age_sec=max_age_sec),
        )

    if item.src == from_:
        assert item.dst == to
        rate = item.quote
        result = amount * item.quote
    else:
        # Not trying to prefer the former case,
        # as there's usually no inverse market.
        assert item.src == to
        assert item.dst == from_
        rate = 1 / item.quote
        result = amount / item.quote

    return dict(result=round(result, 6), rate=round(rate, 12), age_sec=round(age_sec, 3))
