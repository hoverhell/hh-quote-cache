import contextlib
from collections.abc import AsyncGenerator

import sqlalchemy.ext.asyncio

from hh_quote_cache.settings import Settings

TDBPool = sqlalchemy.ext.asyncio.async_sessionmaker[sqlalchemy.ext.asyncio.AsyncSession]
TDBConn = sqlalchemy.ext.asyncio.AsyncSession


def make_db_engine(settings: Settings, echo: bool = False) -> sqlalchemy.ext.asyncio.AsyncEngine:
    """
    Note on `echo`: logging still happens with `echo=False`,
    but with `echo=True` the "sqlalchemy.engine.Engine" logger
    gets an extra stdout handler (sqlalchemy==2.0.23).
    """
    url = settings.db_url
    return sqlalchemy.ext.asyncio.create_async_engine(
        url,
        future=True,
        echo=echo,
    )


@contextlib.asynccontextmanager
async def db_session_acm(settings: Settings) -> AsyncGenerator[TDBPool, None]:
    engine = make_db_engine(settings=settings)

    # expire_on_commit=False will prevent attributes from being expired
    # after commit.
    session_maker = sqlalchemy.ext.asyncio.async_sessionmaker(
        engine,
        autoflush=False,
        expire_on_commit=False,
    )

    try:
        yield session_maker
    finally:
        await engine.dispose()
