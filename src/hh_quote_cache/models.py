import datetime
import decimal

import sqlalchemy
import sqlalchemy.dialects.postgresql
import sqlalchemy.exc
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


class Base(DeclarativeBase):
    pass


class Quote(Base):
    __tablename__ = "quote"

    src: Mapped[str] = mapped_column(sqlalchemy.String, primary_key=True)
    dst: Mapped[str] = mapped_column(sqlalchemy.String, primary_key=True)
    timestamp: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(timezone=True), index=True, primary_key=True
    )
    quote: Mapped[decimal.Decimal] = mapped_column(sqlalchemy.DECIMAL(36, 12))

    # __table_args__ = (sqlalchemy.Index("quote_pkey_idx", src, dst, timestamp, unique=True),)

    @classmethod
    def query_find(cls, symbol_a: str, symbol_b: str, at_ts: datetime.datetime | None = None) -> sqlalchemy.Select:
        query = sqlalchemy.select(cls)
        query = query.where(
            ((cls.src == symbol_a) & (cls.dst == symbol_b)) | ((cls.src == symbol_b) & (cls.dst == symbol_a))
        )
        if at_ts is not None:
            query = query.where(cls.timestamp <= at_ts)
        query = query.order_by(sqlalchemy.desc(cls.timestamp), cls.src, cls.dst)
        query = query.limit(1)
        return query

    @classmethod
    def query_delete_old(cls, cutoff: datetime.datetime) -> sqlalchemy.Delete:
        return sqlalchemy.delete(cls).where(cls.timestamp < cutoff)

    async def save_or_update(self, db: AsyncSession) -> None:
        columns = self.__table__.columns
        key_columns = [col.name for col in columns if col.primary_key]
        data_columns = [col.name for col in columns if not col.primary_key]
        key = {name: getattr(self, name) for name in key_columns}
        data = {name: getattr(self, name) for name in data_columns}
        query = (
            sqlalchemy.dialects.postgresql.insert(self.__class__)
            .values({**key, **data})
            .on_conflict_do_update(index_elements=key_columns, set_=data)
        )
        await db.execute(query)
