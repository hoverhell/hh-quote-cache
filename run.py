#!/usr/bin/env python3
"""
Entry point script.

Deprecated. Run `hh-quote-cache ...` instead.
"""

from hh_quote_cache.main import main

if __name__ == "__main__":
    main()
